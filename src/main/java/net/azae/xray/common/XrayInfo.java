package net.azae.xray.common;

import java.util.ArrayList;
import java.util.List;

public class XrayInfo {
    private List<String> testEnvironments = new ArrayList<>();
    private String summary = "";
    private String description = "This execution is automatically created by JUnit-Xray library (https://gitlab.com/azae/outils/junit-xray)";

    public List<String> getTestEnvironments() {
        return testEnvironments;
    }

    public void setTestEnvironments(List<String> value) {
        testEnvironments = value;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String value) {
        summary = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
