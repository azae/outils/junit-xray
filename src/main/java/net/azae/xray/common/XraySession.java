package net.azae.xray.common;

import net.azae.xray.utils.HttpClient;
import net.azae.xray.utils.XrayHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class XraySession {
    private final static Logger logger = LoggerFactory.getLogger(XraySession.class);

    private XrayTestSuite testSuite;
    private XrayHttpClient httpClient;

    public XraySession() {
        httpClient = new HttpClient();
    }

    public XraySession(XrayHttpClient httpClient) {
        this.httpClient = httpClient;
    }
    public static Optional<String> getXrayToken() {
        String xray_client_secret = getEnv("XRAY_CLIENT_SECRET");
        String xray_client_id = getEnv("XRAY_CLIENT_ID");
        String token = new HttpClient().getToken(buildAuthenticateJson(xray_client_secret, xray_client_id));
        return Optional.of(token);
    }

    public static String getEnv(String env) {
        String variable = System.getenv(env);
        if (variable == null) throw new RuntimeException("Missing " + env + " environment variable");
        return variable;
    }

    public static List<String> extractRunningEnvironment(Map<String, String> env) {
        List<String> runningEnvironment = new ArrayList<>();
        if (env.containsKey("CI"))
            runningEnvironment.add("gitlab");
        else if (env.containsKey("JENKINS_URL")) {
            runningEnvironment.add("jenkins");
        } else {
            runningEnvironment.add("development");
        }
        return runningEnvironment;
    }

    public static String buildAuthenticateJson(String xray_client_secret, String xray_client_id) {
        return "{ " +
                "\"client_id\": \"" + xray_client_id + "\", " +
                "\"client_secret\": \"" + xray_client_secret + "\"" +
                " }";
    }

    public void push(XrayTest test) {
        getTestSuite().putTest(test);
    }

    public XrayTestSuite getTestSuite() {
        return getTestSuite(System.getenv());
    }

    public XrayTestSuite getTestSuite(Map<String, String> env) {
        if (testSuite == null) {
            List<String> runningEnvironment = extractRunningEnvironment(env);
            testSuite = XrayTestSuite.withInfos("Automated test", runningEnvironment);
        }
        return testSuite;
    }

    public void upload() {
        logger.info("Uploading results data to Xray...");

        getTestSuite().extractTestsToUpload()
                .ifPresent(suite -> {
                    getXrayToken().ifPresent(token -> {
                        Mapper.ToJsonAsOptional(suite).ifPresent(json -> {
                            httpClient.publishToXray(token, json);
                        });
                    });
                });
        logger.info("done");
    }

    public List<XrayTest> getTests() {
        return getTestSuite().getTests();
    }

    public XrayTest getTestFromId(String id) {
        return getTestSuite().getTestFromId(id);
    }
}
