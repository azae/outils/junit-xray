package net.azae.xray.common;

import java.util.*;
import java.util.stream.Collectors;

public class XrayTestSuite {
    private Map<String, XrayTest> tests = new HashMap<>();
    private XrayInfo info = new XrayInfo();

    public static XrayTestSuite withInfos(String summary, List<String> envs) {
        XrayTestSuite suite = new XrayTestSuite();
        XrayInfo info = new XrayInfo();
        info.setTestEnvironments(envs);
        info.setSummary(summary);
        suite.setInfo(info);
        return suite;
    }

    public List<XrayTest> getTests() {
        return new ArrayList<>(tests.values());
    }

    public void setTests(List<XrayTest> tests) {
        tests.forEach((test) -> this.tests.put(test.getId(), test));
    }

    public XrayInfo getInfo() {
        return info;
    }

    public void setInfo(XrayInfo info) {
        this.info = info;
    }

    public void putTest(XrayTest test) {
        this.tests.put(test.getId(), test);
    }

    public XrayTest getTestFromId(String uniqueId) {
        if (this.tests.containsKey(uniqueId)) return this.tests.get(uniqueId);
        XrayTest test = new XrayTest();
        test.setId(uniqueId);
        return test;
    }

    public Optional<XrayTestSuite> extractTestsToUpload() {
        List<XrayTest> uploadTests = this.tests.values().stream().filter(XrayTest::isUpload).collect(Collectors.toList());
        if (uploadTests.size() > 0) {
          XrayTestSuite testSuite = new XrayTestSuite();
          testSuite.setTests(uploadTests);
          testSuite.setInfo(info);
          return Optional.of(testSuite);
        }
        return Optional.empty();
    }
}
