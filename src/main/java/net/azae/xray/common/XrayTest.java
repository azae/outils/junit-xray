package net.azae.xray.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = {"id", "upload"})
public class XrayTest {
    private String id;
    private String testKey;
    private XrayStatus status;
    private String comment;
    private boolean upload = false;


    public static XrayTest of(String uniqueId, String testKey) {
        return of(uniqueId, testKey, false);
    }

    public static XrayTest of(String uniqueId, String testKey, boolean upload) {
        XrayTest test = new XrayTest();
        test.setId(uniqueId);
        test.setTestKey(testKey);
        test.setUpload(upload);
        return test;
    }

    public static XrayTest of(String uniqueId, String testKey, boolean upload, XrayStatus status) {
        XrayTest test = XrayTest.of(uniqueId, testKey, upload);
        test.setStatus(status);
        return test;
    }

    public static XrayTest of(String uniqueId, String testKey, boolean upload, XrayStatus status, String comment) {
        XrayTest test = XrayTest.of(uniqueId, testKey, upload, status);
        test.setComment(comment);
        return test;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTestKey() {
        return testKey;
    }

    public void setTestKey(String testKey) {
        this.testKey = testKey;
    }

    public XrayStatus getStatus() {
        return status;
    }

    public void setStatus(XrayStatus status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isUpload() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload = upload;
    }

    @Override
    public String toString() {
        return "XrayTest{" +
                "id='" + id + '\'' +
                ", testKey='" + testKey + '\'' +
                ", status=" + status +
                ", comment='" + comment + '\'' +
                ", upload=" + upload +
                '}';
    }
}
