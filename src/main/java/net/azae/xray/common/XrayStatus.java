package net.azae.xray.common;

import org.junit.platform.engine.TestExecutionResult;

public enum XrayStatus {
    PASSED, FAILED, ABORTED;

    public static XrayStatus fromJunitExecution(TestExecutionResult.Status status) {
        switch (status) {
            case SUCCESSFUL:
                return PASSED;
            case ABORTED:
                return ABORTED;
            default:
                return FAILED;
        }
    }
}
