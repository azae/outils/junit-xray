package net.azae.xray.common;

import net.azae.xray.junit5.XrayExtension;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD})
@Retention(RUNTIME)
@ExtendWith(XrayExtension.class)
public @interface Xray {
    String value();

    boolean upload() default true;
}
