package net.azae.xray.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Optional;

public class Mapper {
    public static String toJson(XrayTestSuite suite) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(suite);
    }

    public static String toJson(XrayTest test) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(test);
    }

    public static XrayTest testFromJson(String s) throws IOException {
        return new ObjectMapper().readValue(s, XrayTest.class);
    }

    public static XrayTestSuite suiteFromJson(String s) throws IOException {
        return new ObjectMapper().readValue(s, XrayTestSuite.class);

    }

    public static Optional<String> ToJsonAsOptional(XrayTestSuite suite) {
        try {
            return Optional.of(toJson(suite));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
