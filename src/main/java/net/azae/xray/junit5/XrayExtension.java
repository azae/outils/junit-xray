package net.azae.xray.junit5;


import net.azae.xray.common.Xray;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.platform.commons.support.AnnotationSupport.findAnnotation;

public class XrayExtension implements BeforeEachCallback {
    public static final String XRAY_TEST_ID = "xray test id";
    public static final String UPLOAD_TO_XRAY = "should upload to xray";
    private final static Logger logger = LoggerFactory.getLogger(XrayExtension.class);

    @Override
    public void beforeEach(final ExtensionContext context) {
        logger.debug("beforeEach for: " + context.getDisplayName());
        findAnnotation(context.getTestMethod(), Xray.class).ifPresent(testCase -> {
            context.publishReportEntry(XRAY_TEST_ID, testCase.value());
            context.publishReportEntry(UPLOAD_TO_XRAY, String.valueOf(testCase.upload()));
        });
    }

}
