package net.azae.xray.junit5;

import net.azae.xray.common.XraySession;
import net.azae.xray.common.XrayStatus;
import net.azae.xray.common.XrayTest;
import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.engine.reporting.ReportEntry;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

public class XrayListener implements TestExecutionListener {
    private final static Logger logger = LoggerFactory.getLogger(XrayListener.class);
    private final XraySession session = new XraySession();

    public XrayListener() {
        logger.info("XrayListener created");
    }

    @Override
    public void reportingEntryPublished(final TestIdentifier testIdentifier, final ReportEntry entry) {
        String uniqueId = testIdentifier.getUniqueId();
        XrayTest test = session.getTestFromId(uniqueId);
        updateXrayTestWithAnnotation(entry, test);
        session.push(test);
    }

    public void updateXrayTestWithAnnotation(ReportEntry entry, XrayTest test) {
        extractXrayTestKey(entry, XrayExtension.XRAY_TEST_ID).ifPresent(testKey -> {
            test.setTestKey(testKey);
            logger.info("Xray test: " + testKey);
        });
        extractXrayTestKey(entry, XrayExtension.UPLOAD_TO_XRAY).ifPresent(up -> {
            test.setUpload(Boolean.parseBoolean(up));
            logger.info("Xray upload: " + up);
        });
    }

    public Optional<String> extractXrayTestKey(ReportEntry entry, String key) {
        Map<String, String> map = entry.getKeyValuePairs();
        if (map.containsKey(key)) {
            return Optional.of(map.get(key));
        }
        return Optional.empty();
    }

    @Override
    public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) {
        if (!testIdentifier.isTest()) {
            return;
        }

        logger.debug("Execution finished: " + testIdentifier.getDisplayName() + " - " +
                testExecutionResult.getStatus().toString());

        XrayStatus status = XrayStatus.fromJunitExecution(testExecutionResult.getStatus());
        String uniqueId = testIdentifier.getUniqueId();
        XrayTest test = session.getTestFromId(uniqueId);
        test.setStatus(status);
        test.setComment(extractComment(testIdentifier, testExecutionResult));
        session.push(test);
    }

    public String extractComment(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) {
        if (testExecutionResult.getThrowable().isPresent()) {
            String message = testExecutionResult.getThrowable().get().getMessage();
            if (testIdentifier.getParentId().isPresent()) {
                return message + testIdentifier.getParentId().get();
            }
            return message;
        }
        return testIdentifier.getDisplayName();
    }

    @Override
    public void testPlanExecutionFinished(TestPlan testPlan) {
        session.upload();
    }

}
