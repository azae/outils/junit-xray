package net.azae.xray.utils;

public interface XrayHttpClient {
    void publishToXray(String token, String inputJson);

    String getToken(String json);
}
