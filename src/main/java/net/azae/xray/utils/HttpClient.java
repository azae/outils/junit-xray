package net.azae.xray.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HttpClient implements XrayHttpClient {
    private final static Logger logger = LoggerFactory.getLogger(HttpClient.class);

    // TODO : get base URL from conf
    String baseUrl = "https://xray.cloud.xpand-it.com";

    public static String jsonPost(String toURL, String data, String token) {
        URL url;
        try {
            url = new URL(toURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            if (token != null)
                conn.setRequestProperty("Authorization", "Bearer " + token);

            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
            writer.write(data);
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                return getResponseBody(conn);
            } else {
                return "Warning, response code: " + responseCode;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Error: impossible case";
    }

    private static String getResponseBody(HttpURLConnection conn) throws IOException {
        String lines = "";
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((line = br.readLine()) != null) {
            lines += line;
        }
        return lines;
    }

    private static String jsonPost(String toURL, String data) {
        return jsonPost(toURL, data, null);
    }

    @Override
    public String getToken(String json) {
        String response = jsonPost(baseUrl + "/api/v1/authenticate", json);
        return response.replace("\"", "");
    }

    @Override
    public void publishToXray(String token, String inputJson) {
        logger.debug("Publish input: " + inputJson);
        String response = jsonPost(baseUrl + "/api/v1/import/execution", inputJson, token);
        logger.debug("Publish response: " + response);
    }
}
