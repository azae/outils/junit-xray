package net.azae.xray.testng;

import net.azae.xray.common.Xray;
import net.azae.xray.common.XraySession;
import net.azae.xray.common.XrayStatus;
import net.azae.xray.common.XrayTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.lang.reflect.Method;
import java.util.UUID;

import static org.junit.platform.commons.support.AnnotationSupport.findAnnotation;

public class XrayListener extends TestListenerAdapter {
    private final static Logger logger = LoggerFactory.getLogger(net.azae.xray.junit5.XrayListener.class);
    private final XraySession session = new XraySession();

    @Override
    public void onTestFailure(ITestResult tr) {
        try {
            buildAndPushXrayTestToSession(tr, XrayStatus.FAILED);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSuccess(ITestResult tr) {
        try {
            buildAndPushXrayTestToSession(tr, XrayStatus.PASSED);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinish(ITestContext testContext) {
        session.upload();
    }

    private void buildAndPushXrayTestToSession(ITestResult tr, XrayStatus status) throws NoSuchMethodException {
        String methodName = tr.getMethod().getMethodName();
        Method testMethod = tr.getInstance().getClass().getMethod(methodName);
        findAnnotation(testMethod, Xray.class).ifPresent(annotation -> {
            String comment = tr.getTestName();
            String uniqueId = UUID.randomUUID().toString();
            XrayTest xrayTest = XrayTest.of(uniqueId, annotation.value(), annotation.upload(), status, comment);
            session.push(xrayTest);
            logger.debug("Add test: " + xrayTest.toString());
        });
    }

    XraySession getSession() {
        return session;
    }
}
