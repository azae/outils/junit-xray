package net.azae.xray.testng;


import net.azae.xray.common.Xray;
import net.azae.xray.testng.XrayListener;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({XrayListener.class})
public class TestNgIntegrationTest {
    @Test
    @Xray(value = "JX-13")
    public void should_report_xray_id_and_status_passed() {
        Assert.assertTrue(true);
    }

    @Test
    @Xray(value = "JX-14")
    public void should_report_xray_id_and_status_failed() {
        //Assert.assertTrue(false);
    }

}
