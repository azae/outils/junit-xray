package net.azae.xray.testng;

import net.azae.xray.common.XraySession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class XrayListenerTest {
    @Test
    public void should_add_failed_test_in_session_onTestFailure() {
        XrayListener xrayListener = new XrayListener();
        XraySession session = xrayListener.getSession();
        Assertions.assertEquals(0, session.getTests().size());
    }

}