package net.azae.xray.common;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class XrayTestSuiteTest {
    @Test
    void should_have_no_test_at_startup() {
        XrayTestSuite suite = new XrayTestSuite();
        assertEquals(0, suite.getTests().size());
    }

    @Test
    void should_add_test() {
        XrayTestSuite suite = new XrayTestSuite();
        suite.putTest(new XrayTest());
        assertEquals(1, suite.getTests().size());
    }

    @Test
    void should_insert_test_only_once() {
        XrayTestSuite suite = new XrayTestSuite();
        XrayTest test = new XrayTest();
        test.setId("unique id");
        suite.putTest(test);
        suite.putTest(test);
        assertEquals(1, suite.getTests().size());
    }

    @Test
    void should_get_new_test_if_empty_suite() {
        XrayTestSuite suite = new XrayTestSuite();
        assertNotNull(suite.getTestFromId("ID-1"));
    }

    @Test
    void should_get_test_if_already_inserted() {
        XrayTestSuite suite = new XrayTestSuite();
        suite.putTest(XrayTest.of("ID-1", "KEY-1"));
        assertEquals("KEY-1", suite.getTestFromId("ID-1").getTestKey());
    }

    @Test
    void should_build_testsuite_with_only_test_with_upload_tag() {
        XrayTestSuite suite = new XrayTestSuite();
        suite.putTest(XrayTest.of("ID-1", "KEY-1", false));
        suite.putTest(XrayTest.of("ID-2", "KEY-2", true));
        assertTrue(suite.extractTestsToUpload().isPresent());
        assertEquals(1, suite.extractTestsToUpload().get().getTests().size());
    }

    @Test
    void should_do_nothing_when_nothing_to_upload() {
        XrayTestSuite suite = new XrayTestSuite();
        suite.putTest(XrayTest.of("ID-1", "KEY-1", false));
        suite.putTest(XrayTest.of("ID-2", "KEY-2", false));
        assertFalse(suite.extractTestsToUpload().isPresent());
    }

    @Test
    void should_create_testSuite_with_informations() {
        XrayTestSuite suite = XrayTestSuite.withInfos("Summary", Collections.singletonList("running env"));
        assertEquals("Summary", suite.getInfo().getSummary());
        assertEquals("running env", suite.getInfo().getTestEnvironments().get(0));
    }
}