package net.azae.xray.common;

import net.azae.xray.common.XrayStatus;
import org.junit.jupiter.api.Test;
import org.junit.platform.engine.TestExecutionResult.Status;

import static net.azae.xray.common.XrayStatus.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class XrayStatusTest {
    @Test
    void should_convert_pass_status() {
        assertEquals(PASSED, XrayStatus.fromJunitExecution(Status.SUCCESSFUL));
    }

    @Test
    void should_convert_fail_status() {
        assertEquals(FAILED, XrayStatus.fromJunitExecution(Status.FAILED));
    }

    @Test
    void should_convert_abort_status() {
        assertEquals(ABORTED, XrayStatus.fromJunitExecution(Status.ABORTED));
    }
}