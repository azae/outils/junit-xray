package net.azae.xray.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.azae.xray.utils.MockHttpClient;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static net.azae.xray.common.XraySession.buildAuthenticateJson;
import static net.azae.xray.common.XraySession.extractRunningEnvironment;
import static org.junit.jupiter.api.Assertions.assertEquals;

class XraySessionTest {

    @Test
    void should_extract_running_gitlab_environment_from_system() {
        Map<String, String> env = new HashMap<>();
        env.put("CI", "true");
        assertEquals("gitlab", extractRunningEnvironment(env).get(0));
    }

    @Test
    void should_extract_running_jenkins_environment_from_system() {
        Map<String, String> env = new HashMap<>();
        env.put("JENKINS_URL", "URL of Jenkins");
        assertEquals("jenkins", extractRunningEnvironment(env).get(0));
    }

    @Test
    void should_extract_running_development_environment_from_system() {
        Map<String, String> env = new HashMap<>();
        env.put("NO_ENV", "reset env");
        assertEquals("development", extractRunningEnvironment(env).get(0));
    }

    @Test
    void should_build_authentication_json() {
        assertEquals("{ \"client_id\": \"client_id\", \"client_secret\": \"client_secret\" }", buildAuthenticateJson("client_secret", "client_id"));
    }

    @Test
    void should_push_new_test_to_session() {
        XraySession session = new XraySession();
        session.push(XrayTest.of("ID", "JX-1"));
        assertEquals(1, session.getTests().size());
    }

    @Test
    void should_upload_tests_marked_to_upload() throws IOException {
        MockHttpClient httpClient = new MockHttpClient();
        XraySession session = new XraySession(httpClient);
        session.push(XrayTest.of("ID-1", "JX-1", true));
        session.upload();
        Map<String, Object> result = new ObjectMapper().readValue(httpClient.getInputJson(), HashMap.class);
        ArrayList<Object> allTests = (ArrayList<Object>) result.get("tests");
        assertEquals(1, allTests.size());
    }
}