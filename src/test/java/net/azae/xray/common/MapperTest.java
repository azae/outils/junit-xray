package net.azae.xray.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.azae.xray.common.Mapper;
import net.azae.xray.common.XrayInfo;
import net.azae.xray.common.XrayTest;
import net.azae.xray.common.XrayTestSuite;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Collections;

import static net.azae.xray.common.XrayStatus.PASSED;
import static org.junit.jupiter.api.Assertions.*;

class MapperTest {
    @Test
    void should_decode_string_to_produce_xray_test() throws IOException {
        String s = "" +
                "{" +
                "      \"testKey\" : \"ID-TEST\"," +
                "      \"comment\" : \"Test was OK\"," +
                "      \"status\"  : \"PASSED\"" +
                "}";
        XrayTest test = Mapper.testFromJson(s);
        assertEquals(PASSED, test.getStatus());
        assertEquals("Test was OK", test.getComment());
        assertEquals("ID-TEST", test.getTestKey());
    }

    @Test
    void should_convert_test_to_json() throws JsonProcessingException {
        String s = "" +
                "{" +
                "\"testKey\":\"ID-TEST\"," +
                "\"status\":\"PASSED\"," +
                "\"comment\":\"Test was OK\"" +
                "}";
        XrayTest test = new XrayTest();
        test.setTestKey("ID-TEST");
        test.setComment("Test was OK");
        test.setStatus(PASSED);
        assertEquals(s, Mapper.toJson(test));
    }

    @Test
    void should_decode_string_to_produce_test_suite() throws IOException {
        String s = "{" +
                "  \"tests\" : [{" +
                "      \"testKey\" : \"ID-TEST\"," +
                "      \"comment\" : \"Test was OK\"," +
                "      \"status\"  : \"PASSED\"" +
                "    }]," +
                "  \"info\": {" +
                "      \"testEnvironments\":[\"gitlab\"]," +
                "      \"summary\" : \"Test from JUnit\"" +
                "    }" +
                "}";
        XrayTestSuite suite = Mapper.suiteFromJson(s);
        assertEquals(Collections.singletonList("gitlab"), suite.getInfo().getTestEnvironments());
        assertEquals("Test from JUnit", suite.getInfo().getSummary());
    }

    @Test
    void should_convert_test_suite_to_json() throws IOException {
        String s = "{" +
                "\"tests\":[{" +
                "\"testKey\":\"ID-TEST\"," +
                "\"status\":\"PASSED\"," +
                "\"comment\":\"Test was OK\"" +
                "}]," +
                "\"info\":{" +
                "\"testEnvironments\":[\"gitlab\"]," +
                "\"summary\":\"Test from Gitlab\"," +
                "\"description\":\"This execution is automatically created by JUnit-Xray library (https://gitlab.com/azae/outils/junit-xray)\""+
                "}" +
                "}";
        XrayTestSuite suite = new XrayTestSuite();
        XrayInfo info = new XrayInfo();
        XrayTest test = new XrayTest();
        test.setStatus(PASSED);
        test.setTestKey("ID-TEST");
        test.setComment("Test was OK");
        suite.putTest(test);
        info.setSummary("Test from Gitlab");
        info.setTestEnvironments(Collections.singletonList("gitlab"));
        suite.setInfo(info);
        assertEquals(s, Mapper.toJson(suite));
    }

    @Test
    void should_return_optional_of_Json_when_no_error() {
        XrayTestSuite suite = XrayTestSuite.withInfos("Summary", Collections.singletonList("env"));
        assertTrue(Mapper.ToJsonAsOptional(suite).isPresent());
    }

    @Test
    void should_return_empty_optional_when_error() {
        TestSuiteTheyTriggerError error = new TestSuiteTheyTriggerError();
        assertFalse(Mapper.ToJsonAsOptional(error).isPresent());
    }

    private class TestSuiteTheyTriggerError extends XrayTestSuite {
        private final TestSuiteTheyTriggerError self = this;

        public TestSuiteTheyTriggerError getSelf() {
            return self;
        }
    }


}