package net.azae.xray.junit5;

import net.azae.xray.common.Xray;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("integration")
public class JUnit5IntegrationTest {
    @Test
    @Xray(value = "RSD-934")
    void should_report_xray_id_and_status_passed() {
        assertTrue(true);
    }

    @Test
    @Xray(value = "JX-4")
    void should_report_xray_id_and_status_failed() {
        //Assertions.fail();
    }

    @Test
    @Xray(value = "JX-12")
    void should_be_green() {
        assertTrue(true);
    }
}
