package net.azae.xray.junit5;

import net.azae.xray.common.XrayTest;
import org.junit.jupiter.api.Test;
import org.junit.platform.engine.reporting.ReportEntry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class XrayListenerTest {

    @Test
    void should_extract_testkey_annotations_information() {
        XrayListener listener = new XrayListener();
        ReportEntry entry = ReportEntry.from(XrayExtension.XRAY_TEST_ID, "ID-XRAY");
        XrayTest test = new XrayTest();
        listener.updateXrayTestWithAnnotation(entry, test);
        assertEquals("ID-XRAY", test.getTestKey());
    }

    @Test
    void should_extract_upload_annotations_information() {
        XrayListener listener = new XrayListener();
        ReportEntry entry = ReportEntry.from(XrayExtension.UPLOAD_TO_XRAY, "true");
        XrayTest test = new XrayTest();
        listener.updateXrayTestWithAnnotation(entry, test);
        assertTrue(test.isUpload());
    }

}