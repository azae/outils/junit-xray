package net.azae.xray.utils;

public class MockHttpClient implements XrayHttpClient {
    private String inputJson;

    @Override
    public void publishToXray(String token, String inputJson) {
        this.inputJson = inputJson;
    }

    @Override
    public String getToken(String json) {
        return "my token";
    }

    public String getInputJson() {
        return inputJson;
    }

}
