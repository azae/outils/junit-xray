package net.azae.xray.utils;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import net.azae.xray.utils.HttpClient;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HttpClientTest {
    public static final int PORT = 8500;
    static HttpServer server;

    @BeforeAll
    static void start_mock_server() throws IOException {
        server = HttpServer.create(new InetSocketAddress(PORT), 0);
        server.createContext("/", authenticate());
        server.start();
    }

    private static HttpHandler authenticate() {
        return exchange -> {
            byte[] response = "\"my-token\"".getBytes();
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            OutputStream os = exchange.getResponseBody();
            os.write(response);
            os.close();
        };
    }

    @AfterAll
    static void stop_mock_server() {
        server.stop(9);
    }

    @Test
    void should_get_token() {
        HttpClient httpClient = new HttpClient();
        httpClient.baseUrl = "http://localhost:"+PORT+"/";
        String response = httpClient.getToken("json with client_id and client_secret");
        assertEquals("my-token", response);
    }

}